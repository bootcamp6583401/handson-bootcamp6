import java.util.Scanner;

public class Handson1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tulis array pertama: ");
        int[] firstNumbers = getArray(scanner);

        int total = 0;
        Integer max = null;
        Integer min = null;
        for (int i : firstNumbers) {
            System.out.println("Calculate");
            total += i;
            if (max == null) {
                max = i;
            } else if (i >= max) {
                max = i;
            }

            if (min == null) {
                min = i;
            } else if (i <= min) {
                min = i;
            }
        }

        double average = (double) total / firstNumbers.length;

        System.out
                .println(String.format("Length: %d\nTotal: %d\nRata-rata: %.2f\nMax: %d\nMin: %d", firstNumbers.length,
                        total, average, max, min));

        System.out.println("Tulis array pertama: ");

        int[] secondNumbers = getArray(scanner);

        int[] combinedArray = new int[firstNumbers.length + secondNumbers.length];

        for (int i = 0; i < combinedArray.length; i++) {
            if (i < firstNumbers.length) {
                combinedArray[i] = firstNumbers[i];
            } else {
                combinedArray[i] = secondNumbers[i - firstNumbers.length];
            }
        }

        for (int i : combinedArray) {
            System.out.print(i + " ");
        }

        scanner.close();
    }

    public static int[] getArray(Scanner scanner) throws NumberFormatException {
        String input = scanner.nextLine();

        String[] numberInputs = input.split(" ");

        int[] numbers = new int[numberInputs.length];

        try {
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = Integer.parseInt(numberInputs[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Bukan angka");
            System.exit(0);
        }

        return numbers;
    }
}