import java.util.Scanner;

public class Handson5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] firstNumbers = getArray(scanner);

        Integer max = null;
        Integer min = null;
        for (int i : firstNumbers) {
            if (max == null) {
                max = i;
            } else if (i >= max) {
                max = i;
            }

            if (min == null) {
                min = i;
            } else if (i <= min) {
                min = i;
            }
        }
        System.out.println(String.format("Max: %d\nMin: %d", max, min));

        scanner.close();
    }

    public static int[] getArray(Scanner scanner) throws NumberFormatException {
        String input = scanner.nextLine();

        String[] numberInputs = input.split(" ");

        int[] numbers = new int[numberInputs.length];

        try {
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = Integer.parseInt(numberInputs[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Bukan angka");
            System.exit(0);
        }

        return numbers;
    }
}