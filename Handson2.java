import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Handson2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String filePath = "test.txt";

        try {
            String content = new String(Files.readAllBytes(Paths.get(filePath)));

            System.out.print("Kata yang dicari: ");
            String search = scanner.nextLine();

            if (content.contains(search)) {
                content += "\nKETEMU!";
            } else {
                content += "\nTIDAK KETEMU";
            }

            Files.write(Paths.get(filePath), content.getBytes());

            System.out.println("Selesai!");

        } catch (IOException e) {
            System.out.println("Error!: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error!");
        }
        scanner.close();
    }
}
