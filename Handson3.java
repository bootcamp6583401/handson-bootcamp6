import java.util.Scanner;

public class Handson3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double[] doubleArray = { 1.2, 3.8, 2.0 };

        System.out.print("Enter int: ");

        int intNum = scanner.nextInt();

        double totalArray = 0;

        for (double d : doubleArray) {
            totalArray += d;
        }

        totalArray += intNum;

        double average = totalArray / (doubleArray.length + 1);

        System.out.println("Rata-rata: " + average);

        scanner.close();

    }
}
